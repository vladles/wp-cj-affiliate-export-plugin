<?php
/**
 * Plugin Name: CJAffiliate Export plugin 
 * Plugin URI:
 * Description:  Export product in CJAffiliate system
 * Version: 1.0
 * Author: Cimpleo <Vlad Lesovskiy>
 * Author URI: http://cimpleo.com
 * Requires at least: 4.4
 * Tested up to: 4.9
 *
 *
 * @package CJAffiliate_plugin
 * @author Cimpleo
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

final class CJAffiliate_plugin {

	/**
	 * Plugin version
	 * @var string
	 */
	public $version = '1.1';

	/**
	 * Plugin __construct 
	 */
	public function __construct() {
		$this->init_hooks();
		$this->includesFiles();
		$this->initDefinesConstants();
	}

	/**
	 * Hook into actions and filters.
	 *
	 */
	private function init_hooks() {
	// Plugin actions
		register_activation_hook( __FILE__, array( $this, 'installPlugin' ) );
		register_deactivation_hook( __FILE__,  array( $this, 'deactivatePlugin' ) );	

	// Actions
		add_action( 'admin_init', array( $this, 'registerSettingsFields' ) );
		add_action( 'admin_menu', array( $this, 'addMenuItem' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'adminEnqueueFiles' ) );
	}

	/**
	 * Include required core files.
	 */
	public function includesFiles() {
		include_once( $this->plugin_path().'/include/classes/CjAjaxRequests.php' );
		include_once( $this->plugin_path().'/include/classes/CjExport.php' );
		include_once( $this->plugin_path().'/include/classes/CjItemRecord.php' );
	}

	/**
	 * Init Constants for plugin.
	 * 
	 */
	private function initDefinesConstants() {
		$upload_dir = wp_upload_dir();

		$this->define( 'CJAFFILIATE_VERSION', $this->version );
		$this->define( 'CJAFFILIATE_UPLOADS', $upload_dir['basedir'] . '/CJ-Affilate/' );
		$this->define( 'CJAFFILIATE_UPLOADS_URL', $upload_dir['baseurl'] . '/CJ-Affilate' );
	}

	/**
	 * 
	 * Register settings fields for Export Settings Page
	 * Initilize Callback method for CRON
	 * 
	 */
	public function registerSettingsFields() {
		register_setting( 'optionsExport_group', 'CJAffiliate_plugin_export', array($this, 'settingsFieldsCallback' ) );
		register_setting( 'optionsExportTransfer_group', 'CJAffiliate_plugin_exportTransfer', array( $this, 'settingsFieldsCallback' ) );
	}

	/**
	 * 
	 * Install Plugin Hook.
	 * Create Files and Folders for export files.
	 * 
	 */
	public static function installPlugin() {
		if ( ! current_user_can( 'activate_plugins' ) )
			return;
	// Create Folder
		$upload = wp_upload_dir();
		$upload_dir = $upload['basedir'];
		$upload_dir = $upload_dir . '/CJ-Affilate/';
		wp_mkdir_p( $upload_dir );
	// Add default settings
		$exportOptions = array( 'cid' => '', 'subid' => '', 'processtype' => 'update', 'aid' => '', 'cron_activate' => false );
		$status = update_option( 'CJAffiliate_plugin_export', $exportOptions );

		$exportTransferOptions = array( 'ftp_host' => '', 'ftp_port' => '', 'ftp_login' => '', 'ftp_pwd' => '', 'ftp_port'	=> '' );
		update_option( 'CJAffiliate_plugin_exportTransfer', $exportTransferOptions );

	// WP-Cron. Clear event
		wp_clear_scheduled_hook( 'cron_cj_export' );
	}

	/**
	 * 
	 * Deactivate Plugin hook.
	 * Remove options.
	 * 
	 */
	public static function deactivatePlugin() {
		if ( ! current_user_can( 'activate_plugins' ) )
			return;
		delete_option( 'CJAffiliate_plugin_export' );
		delete_option( 'CJAffiliate_plugin_exportTransfer' );

	// Cron clear event
		wp_clear_scheduled_hook( 'cron_cj_export' );
	}


	/**
	 * Enqueue Scripts and Styles 
	 * 
	 */
	public function adminEnqueueFiles() {
	// Custom style
		wp_enqueue_style( 'CJ-style.css', $this->plugin_url('/assets/main.css') );
	// Script
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'CJ-AffiliateExport.js', $this->plugin_url('/assets/main.js'), array( 'jquery' ) );
	// Set localize variable
		wp_localize_script( 'CJ-AffiliateExport.js', 'CJExport', array( 
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'noncefield'	=> wp_create_nonce('cj-export-nonce')
		));
	}

	/**
	 * Add Item In Admin Menu.
	 * 
	 */
	public function addMenuItem() {
		add_submenu_page( 'tools.php', 'СJ Export Product', 'CJ export', 'manage_options', 'cjexport',  array( $this, 'renderPage' ) );
	}

	/**
	 * Render export page
	 * 
	 */
	public function renderPage() {
		include_once( $this->plugin_path().'/include/page-export.php' );
	}



	/**
	 * [sendfile_ftp description]
	 * @param  string $filepath
	 * @return Bool
	 */
	public static function sendfile_ftp( $filepath ) {
	// Get ftp options
		$ftp_options = get_option( 'CJAffiliate_plugin_exportTransfer' );
	// Ftp connect;
		if ( !$ftp_options && empty( $ftp_options['ftp_host'] )  )
			return false;

		$conn_id = ftp_connect( $ftp_options['ftp_host'] );

		if ( $conn_id ) {
			$login_result = ftp_login($conn_id, $ftp_options['ftp_login'] , $ftp_options['ftp_pwd']);

			if ( $login_result ) {
				$basename = basename( $filepath );
				if (ftp_put($conn_id, $basename, $filepath, FTP_ASCII)) {
					ftp_close($conn_id);
					return [
						'type'	=> 'ftp', 
						'returnValue' => true
					];
				}
			}
		}
		return false;
	}

	/**
	 * [sendfile_email description]
	 * @param  string $filepath 
	 * @param  string $email
	 * @return Bool (true/false)
	 */
	public static function sendfile_email( $filepath, $email ) {
		if ( !isset( $email ) )
			return false;

		$attachments = array( $filepath );
		$email_admin = get_bloginfo('admin_email');
		$headers = 'From: My Name <'. $email_admin .'>' . "\r\n";

		$result = wp_mail($email, 'File CJAffiliate', 'You received an export file for the system CJAffiliate', $headers, $attachments);

		return [
			'type'		=> 'email',
			'returnValue'	=> $result
		];
	}


	public static function sendfile_download( $filepath ) {
		return [
			'type' => 'download',
			'returnValue' => $filepath
		];

	}

	public function settingsFieldsCallback( $options ) {
		if ( !isset( $options['cron_activate'] ) ) {
			wp_clear_scheduled_hook( 'cron_cj_export' );

		}else {
			foreach( $options as $name => & $val ){
		// Check cron activate and create schudule.
				if ( $name == "cron_activate" && $val == 1  && !empty($options['cron_schedules'] ) ) {
			// Clear all event
					wp_clear_scheduled_hook( 'cron_cj_export' );

			// Create new event
					if ( $options['cron_schedules_time'] ) {
						$time = get_gmt_from_date( date( 'Y-m-d H:i:s', $options['cron_schedules_time'] ), 'U' ); 
					}else {
						$time = time();
					}
					wp_schedule_event( $time, $options['cron_schedules'], 'cron_cj_export');
				}	
			}

		}

		return $options;
	}


	/**
	 * Define constant if not already set.
	 *
	 * @param  string $name
	 * @param  string|bool $value
	 */
	private function define( $name, $value ) {
		if ( ! defined( $name ) ) {
			define( $name, $value );
		}
	}

	/**
	 * Get the plugin url.
	 * @return string
	 */
	public function plugin_url( $path ) {
		return untrailingslashit( plugins_url( $path, __FILE__ ) );
	}

	/**
	 * Get the plugin path.
	 * @return string
	 */
	public function plugin_path() {
		return untrailingslashit( plugin_dir_path( __FILE__ ) );
	}
	

}
new CJAffiliate_plugin();	
