===  CJAffiliate Export plugin  ===
Contributors: cimpleo, vladles
Donate link: http://cimpleo.com
Tags: CJAffiliate, export, file, xml, csv
Requires at least: 4.4
Tested up to: 4.7
Stable tag: 4.3
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Export product in CJAffiliate system. 

== Description ==

This is the long description.  No limit, and you can use Markdown (as well as in the following sections).

For backwards compatibility, if this section is missing, the full length of the short description will be used, and
Markdown parsed.


== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress
1. Use the Settings->Plugin Name screen to configure the plugin
1. (Make your instructions match the desired user flow for activating and installing your plugin. Include any steps that might be needed for explanatory purposes)


== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.


== Screenshots ==
1. `/assets/screenshots/1.png` 


== Changelog ==

= 1.0 =
* First deploy

== Upgrade Notice ==

= 1.0 =
First deploy

== Arbitrary section ==



== A brief Markdown Example ==

