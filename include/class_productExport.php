<?php 
/**
 *
 *
 *
 * @package CJAffiliate_plugin
 * @author Cimpleo
 */


class productExport {


	public $currency;
	public $_wc;

	/**
	 * [__construct description]
	 */
	function __construct() {
		$this->init_hooks();
	}

	/**
	 * [init_hooks description]
	 * 
	 */
	private function init_hooks(){
		// Ajax actions
		add_action( 'wp_ajax_plugin_cj_export', array( $this, 'ajax_exportRequest' ) );
		add_action('cron_cj_export', array( $this, 'cron_cj_export_action' ) );
	}

	/**
	 * [ajax_exportRequest description]
	 * @return [type] [description]
	 */
	public function ajax_exportRequest() {
	// Check all 
		if ( !wp_verify_nonce( $_POST['query_data']['_wpnonce'], 'exportCJ_plugin') )
			wp_send_json_error();

		// check_ajax_referer( 'exportCJ_plugin', 'nonce' );
		$current_fileName = $_POST['query_data']['exportfile'];

		$dataQuery = $_POST['query_data'];



		if ( !isset($dataQuery['file_type']) )
			wp_send_json_error( 'Please check fileType' );

	// Set var.	
		$this->_wc = new WC_Product_Factory();  
		$this->currency = get_woocommerce_currency();

	// Generate args for query POST data!!!
		$query_args = array(
			'posts_per_page'	=> 10,
			'post_type' 		=> 'product',
			'offset'			=> $_POST['offset']
		);		
		error_log( print_R( $query_args, true) );

		switch ( $dataQuery['file_type'] ) {
			case 'xml':
			$workFile = $this->generateXML_file( $query_args, $current_fileName );
			break;
			case 'csv':
			$workFile = $this->generateCSV_file( $query_args );
			break;
			default:
			$workFile = false;
			break;
		}

		if ( $workFile ) {
			switch ($dataQuery['transfer_options']) {
				case 'ftp':
				$ajaxResult = $this->sendFileVia_ftp( $workFile );
				break;
				case 'email':
				$ajaxResult = array(
					'type' => 'email',
					'process'	=> $this->sendFileVia_email( $workFile, $dataQuery['send_to_email'] )	
				);
				break;
				case 'download':
				$basename = basename( $workFile );
				$ajaxResult = array(
					'type' => 'file',
					'process'	=> CJAFFILIATE_UPLOADS_URL.'/'.$basename
				);
				break;
				default:
					# code...
				break;
			}
		}

		if ( isset( $ajaxResult ) ) {
			if ( $ajaxResult['process'] != false ) {				
				wp_send_json_success( $ajaxResult );
			}else {
				wp_send_json_error();
			}	
		}
	}

	public function get_XMLCatalog( $filename ) {
		$filepath = CJAFFILIATE_UPLOADS . $filename;

		if ( file_exists( $filepath ) ) {
			$xmlCatalog = $this->get_xml_file( $filepath );
		}else {
			$xmlCatalog = $this->create_xml_file( $filepath );
		}
		error_log(print_R(count($xmlCatalog->product), true));
		return $xmlCatalog;
	}

	public function create_xml_file( $filepath ) {
		include('sampleXML.php');
		$xmlCatalog = new SimpleXMLElement(  $xmlstr );
	// Set settings 
		$settings = get_option('CJAffiliate_plugin_export');
		$array_options = array( 'cid', 'subid', 'aid', 'processtype' );
		if ( $settings ) {
			$xmlHeader = $xmlCatalog->addChild('header');
			foreach ($settings as $key => $value) {
				if ( in_array($key, $array_options ) && !empty( $value ) ) {
					$xmlHeader->addChild( $key, $value );
				}
			}
			return $xmlCatalog;
		}		
		return false;
	}

	public function get_xml_file( $filepath ) {
		$xml = simplexml_load_file( $filepath );

		return $xml;
	}
	/**
	 * [sendFileVia_ftp description]
	 * @param  [type] $file [description]
	 * @return [type]       [description]
	 */
	public function sendFileVia_ftp( $file ) {
		if ( !isset( $file ) )
			return false;

	// Get ftp options
		$ftp_options = get_option( 'CJAffiliate_plugin_exportTransfer' );
	// Ftp connect;
		if ( !$ftp_options && empty( $ftp_options['ftp_host'] )  )
			return false;

		$conn_id = ftp_connect( $ftp_options['ftp_host'] );

		if ( $conn_id ) {

			$login_result = ftp_login($conn_id, $ftp_options['ftp_login'] , $ftp_options['ftp_pwd']);

			if ( !$login_result )
				wp_send_json_error( 'Login failed' );


			$basename = basename( $file );
			if (ftp_put($conn_id, $basename, $file, FTP_ASCII)) {
				return true;
			} else {
				return false;
			}
			ftp_close($conn_id);
		}
	}

	public function sendFileVia_email(  $file, $email ) {
		if ( !isset( $email ) )
			return false;

		$attachments = array( $file );
		$email_admin = get_bloginfo('admin_email');
		$headers = 'From: My Name <'. $email_admin .'>' . "\r\n";

		$result = wp_mail($email, 'File CJAffiliate', 'You received an export file for the system CJAffiliate', $headers, $attachments);

		return $result;
	}


	/**
	 * [generateXML_file description]
	 * @param  [type] $args [description]
	 * @return [type]       [description]
	 */
	public function generateXML_file( $args, $filename ) {
	// Check args!
		if ( !isset( $args ))
			return false;

	// Generate query
		$query = new WP_Query( $args );
		error_log( 'POSTS COUNT FROM query - '. print_R( $query->post_count, true ) );
		if ( $query->have_posts() ) {
		// Create new document 
			$xmlCatalog = $this->get_XMLCatalog( $filename );
			if ( !isset( $xmlCatalog ) )
				return false;

			while ( $query->have_posts() ) { $query->the_post();

			// Get wc product object
				$post_id = get_the_ID();
				$keywords_array = array();

				if ( empty( $this->_wc ) ) {
					$this->_wc = new WC_Product_Factory();  
					$this->currency = get_woocommerce_currency();
				}
				$productObject = $this->_wc->get_product( $post_id );

			// Set product Item
				$desc = $this->substr_xml( get_the_content(), 3000 );
				$stock = $productObject->is_in_stock() ? 'Yes' : 'No';
				

				$artist_name = wp_get_post_terms( $post_id, 'artists', array('fields'=>'names') );
				$city = get_post_meta( $post_id, 'City', true );
				if ( isset( $city ) && !empty( $city ) )
					$keywords_array['city'] = $city;


				$country = get_post_meta( $post_id, 'country', true );
				if ( isset( $country ) && !empty( $country ) )
					$keywords_array['country'] = $country;

				$venue = get_post_meta( $post_id, 'Venue', true );
				if ( isset( $venue ) && !empty( $venue ) )
					$keywords_array['stadium'] = $venue;

				$date = get_post_meta( $post_id, '_date', true );
				if ( $time_date = strtotime( $date ) ) {
					$date_format = date( 'Y-m-d', strtotime( $time_date  ) );
					if ( $date_format ) {
						$keywords_array['event_date'] = $date_format;
					}
				}

				$event_type_list = wp_get_post_terms( $post_id, 'product_cat', array( 'fields' => 'names' ));
				if ( $event_type_list ) {
					$event_type = implode(">", $event_type_list);
					if ( !empty( $event_type ) ) {
						$keywords_array['event_type'] = $event_type;
					}
				}

				if ( !empty( $keywords_array ) ) {
					$keywords_json = json_encode($keywords_array);
				}


				$productItem = array(
					'name'			=> isset($artist_name[0]) ? $artist_name[0] : '',
					'keywords'		=> (!is_array($keywords_json)) ? $keywords_json : '',
					'description'	=> $this->clean( $desc ),
					'sku'			=> empty( $productObject->get_sku() ) ? $post_id : $productObject->get_sku(),
					'byurl'			=> htmlspecialchars( get_the_permalink() ),
					'price'			=> 0,
					'currency'		=> $this->currency,
					'instock'		=> $stock,
					'available'		=> 'Yes'
				);
				isset( $imageurl ) ? $productctItem['imageurl'] = $imageurl : '';	



			// Set Node
				$xmlProduct = $xmlCatalog->addChild('product');

			// Set atribute for xml file	
				if ( $productItem ) {
					foreach ($productItem as $key => $value) {
						$xmlProduct->addChild( $key, htmlspecialchars( $value ) );
					}
				}
			// save meta field for product 
				//update_post_meta( $post_id, '_is_cj_export', 'true' );

		    // Check product for variable type 		
				// if ( $productObject->is_type( 'variable' ) ) {
				// 	$args = array(
				// 		'post_type'      => 'product_variation',
				// 		'post_status'   => array( 'private', 'publish' ),
				// 		'numberposts'   => -1,
				// 		'orderby'       => 'menu_order',
				// 		'order'         => 'asc',
				// 		'post_parent'   => $post_id
				// 	);

				// 	$variations = get_posts( $args ); 
				// 	if ( !empty($variations) ) {
				// 		foreach ( $variations as $post ) {
				// 			$this->add_variationsProduct_xml( $xmlCatalog, $post, $keywords_array );
				// 		}
				// 	}
				// }

			} // EndWhile

			$filepath = CJAFFILIATE_UPLOADS . $filename;

			// get xml string and output as formatted
      // format of the XML is visible on a line by line basis per field
			if ( $xmlString = $xmlCatalog->asXML() ) {

				$dom = new DOMDocument();
				$dom->preserveWhiteSpace = false;
				$dom->formatOutput = true;
				$dom->loadXML($xmlString);
				$formattedXML = $dom->saveXML();

				$fp = fopen($filepath,'w+');
				fwrite($fp, $formattedXML);
				fclose($fp);

				return $filepath;
			}
		} // EndIF
		return false;
	}

	/**
	 * [add_variationsProduct_xml description]
	 * @param [type] $xmlCatalog [description]
	 * @param [type] $post       [description]
	 */
	private function add_variationsProduct_xml( $xmlCatalog = false, $post, $keywords_array  ) {
		if ( $xmlCatalog == false )
			return false;

		// Get wc product object
		$post_id = $post->ID;
		$productObject = new WC_Product_Variation( $post_id ); 


		$varibale_taxonomy = 'pa_seat-location';
		$variable_meta = get_post_meta($post_id, 'attribute_'.$varibale_taxonomy, true);
		if ( $variable_meta ) {
			$varibale_term = get_term_by('slug', $variable_meta, $varibale_taxonomy);
			if ( !empty( $varibale_term ) ) {
				$keywords_array['ticket_type'] = $varibale_term->name;
			}
		}

		if ( !empty( $keywords_array ) ) {
			$keywords_json = json_encode($keywords_array);
		}

		$imageurl = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ) );
		$stock = $productObject->is_in_stock() ? 'Yes' : 'No';

		
		$variableItem = array(
			'name'			=> $this->clean($this->substr_xml( $post->post_title, 160 )),
			// 'keywords'		=> isset($keywords_json) ? $keywords_json : '',
			'keywords'		=> 'test',
			'description'	=> '',
			'sku'			=> empty( $productObject->get_sku() ) ? $post->post_parent . '-'. $post_id  : $productObject->get_sku(),
			'price'			=> $productObject->get_price(),
			'byurl'			=> '123',
			'currency'		=> $this->currency,
			'instock'		=> $stock,
			'available'		=> 'No',
		);
		isset( $imageurl ) ? $variableItem['imageurl'] = $imageurl[0] : '';	
	// Set Node
		$xmlProduct = $xmlCatalog->addChild('product');
		if ( $variableItem ) {
			foreach ( $variableItem as $key => $value ) {
				$xmlProduct->addChild( $key, htmlspecialchars($value) );
			}
		}
		return $xmlCatalog;
	}

	/**
	 * [generate<CSV></CSV>_file description]
	 * @param  [type] $args [description]
	 * @return [type]       [description]
	 */
	public function generateCSV_file( $args ) {
	// Check args!
		if ( !isset( $args ))
			return false;


	// CSV parameters
		$parameters_array = array(
			'NAME', 'KEYWORDS', 'DESCRIPTION', 'SKU', 'BUYURL',
			'AVAILABLE', 'IMAGEURL', 'PRICE', 'CURRENCY',
			'INSTOCK', 'STARTDATE'
		);
		$parameters = implode( '|', $parameters_array );

		$query = new WP_Query( $args );
		if ( $query->have_posts() ) {
		// Create file
			$filename = 'csvExpot'. date('Ymd') .'.csv';
			$csvFile = CJAFFILIATE_UPLOADS. $filename;

		// Setings 
			$settings = get_option('option_export_name');
			if ( $settings ) {
				foreach ($settings as $key => $value) {
					if ( !empty( $value ) ) {
						$string = '&'. $key. '='. $value;
						file_put_contents( $csvFile , $string, FILE_APPEND );
					}
				}
			// Set  parameters in file
				if ( $parameters ) {
					file_put_contents( $csvFile , '&PARAMETERS=' . $parameters, FILE_APPEND );
				}
			}	

			
			while ( $query->have_posts() ) { $query->the_post();
			// Create object 
				$post_id = get_the_ID();
				$productObject = $this->_wc->get_product( $post_id );

			// Set productItem array 
				$name = $this->substr_xml( get_the_title(), 160 );
				$name = str_replace('"', '', $name);

				$desc = $this->substr_xml( get_the_content(), 3000 );
				if ( $desc == false ) {
					$desc = 'Description is missing,';
				}else {
					$desc = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $desc);
					$desc = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $desc);
				}

				$stock = $productObject->is_in_stock() ? 'Yes' : 'No';

				$keywords_list =  wp_get_post_terms( $post_id , 'product_cat', array("fields" => "names"));
				$keywords = implode(">", $keywords_list);

				$productItem = array(
					'name'			=> '"'.$name.'"',
					'keywords'		=> '"'.$name.'"',
					'description'	=> '"'.$desc.'"',
					'sku'			=> empty( $productObject->get_sku() ) ? $post_id : $productObject->get_sku(),
					'buyurl'		=> htmlspecialchars( get_the_permalink() ),
					'price'			=> 0,
					'currency'		=> $this->currency,
					'startdate'		=> get_the_date( 'n/j/Y' ),
					'instock'		=> $stock,
					'available'		=> 'Yes',
					'advertisercategory' => '"'.$keywords.'"'
				);

				$imageurl = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ) );

				isset( $imageurl ) ? $productItem['imageurl'] = $imageurl[0] : '';	


			// String for put in file
				$resultItemString = '';

				if ( $productItem ) {
					foreach ( $productItem as $key => $value ) {
						$resultItemString .= $value . ',';
					}
				}

			// Put in file one item
				if ( !empty( $resultItemString ) ) {
					file_put_contents( $csvFile, $resultItemString, FILE_APPEND );
				}

		    // Check product for variable type 		
				if ( $productObject->is_type( 'variable' ) ) {
					$args = array(
						'post_type'      => 'product_variation',
						'post_status'   => array( 'private', 'publish' ),
						'numberposts'   => -1,
						'orderby'       => 'menu_order',
						'order'         => 'asc',
						'post_parent'   => $post_id
					);

					$variations = get_posts( $args ); 
					foreach ( $variations as $post ) {
						$this->add_variationsProduct_csv( $csvFile, $post );
					}
				}				

			} // EndWhile;

			if ( $csvFile )
				return $csvFile;

		} // EndIf;
		return false;
	}

	/**
	 * [add_variationsProduct_csv description]
	 * @param boolean $csvFile [description]
	 * @param [type]  $post    [description]
	 */
	private function add_variationsProduct_csv( $csvFile = false, $post ) {
		if ( $csvFile == false )
			return false;

		// Get wc product object
		$post_id = $post->ID;
		$productObject = new WC_Product_Variable( $post->post_parent ); 

		$name = $this->substr_xml( $post->post_title, 160 );
		$name = str_replace('"', '', $name);

		$desc = $this->substr_xml( get_the_content(), 3000 );
		if ( $desc == false ) {
			$desc = 'Description is missing,';
		}else {
			$desc = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $desc);
			$desc = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $desc);
		}

		$varibale_taxonomy = 'pa_seat-location';
		$variable_meta = get_post_meta($post_id, 'attribute_'.$varibale_taxonomy, true);
		$varibale_term = get_term_by('slug', $variable_meta, $varibale_taxonomy);
		$imageurl = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ) );
		$stock = $productObject->is_in_stock() ? 'Yes' : 'No';


		$variableItem = array(
			'name'			=> '"'.$name.'"',
			'keywords'		=> '"'.$name.'"',
			'description'	=> '"'.$desc.'"',
			'sku'			=> empty( $productObject->get_sku() ) ? $post_id : $productObject->get_sku(),
			'buyurl'		=> htmlspecialchars( $post->guid ),
			'price'			=> $productObject->get_price(),
			'currency'		=> $this->currency,
			'startdate'		=> get_the_date( 'n/j/Y', $post_id ),
			'instock'		=> $stock,
			'available'		=> 'Yes',
			'advertisercategory' => '"'.$varibale_term->name.'"'
		);

		isset( $imageurl ) ? $variableItem['imageurl'] = $imageurl[0] : '';	


	// String for put in file
		$resultItemString = '';

		if ( $variableItem ) {
			foreach ( $variableItem as $key => $value ) {
				$resultItemString .= $value . ',';
			}
		}

	// Put in file one item
		if ( !empty( $resultItemString ) ) {
			file_put_contents( $csvFile, $resultItemString, FILE_APPEND );
		}
		return $csvFile;
	}



	/**
	 * [cron_cj_export_action description]
	 * @return [type] [description]
	 */
	public function cron_cj_export_action() {
		$query_args = array(
			'post_type' => 'product',
			'meta_query'=> array(
				'product_date' =>array(
					'key'	=> 'Date',
					'value'	=> date( 'd/m/Y' ),
					'compare'=> '>='
				)
			)
		);		


		if ( $query_args ) {
			$workFile = $this->generateXML_file( $query_args );
			error_log( 'Error: '.print_R( $workFile, true ) );
			if ( $workFile ) {
				$ajaxResult = $this->sendFileVia_ftp( $workFile );
				if ( $ajaxResult['process'] != false  )
					return true;
			}
		}
		return false;
	}


	/**
	 * [substr_xml description]
	 * @param  [type] $string [description]
	 * @param  [type] $chars  [description]
	 * @return [type]         [description]
	 */
	private function substr_xml( $string, $chars ) {
		if ( empty( $string ) )
			return false;

		$decode_string = html_entity_decode( $string );
		$string = substr( $decode_string, 0, $chars );
		return $string;
	}

	private function clean($string) {
	    $string = preg_replace('#[^A-Za-z0-9\. -)(]+#', '', $string); // Removes special chars.
	    return preg_replace('#[\\s-]+#', ' ', $string); // Removes multiple spaces chars.
	}

}

new productExport();